Bowtie2 Align GeneFlow App
==========================

Version: 2.4.1-02

This GeneFlow2 app wraps the Bowtie2 Align tool.

Inputs
------

1. input: Input FASTQ File

2. pair: Input FASTQ Pair

3. reference: Reference Index Directory 

Parameters
----------

1. threads: CPU Threads - Number of CPU threads used for aligning sequences. Default: 2

2. output: Output SAM File - Default: output.sam
